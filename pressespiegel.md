---
layout: page
title: Die Dresdner Studentenclubs in der Presse
menu: Pressespiegel
order: 3
---

[DNN: Kultkneipe. Prost Mahlzeit: Bierstube in der Neuen Mensa Dresden soll im April 2025 öffnen. 20.11.2024](https://www.dnn.de/lokales/dresden/dresden-bierstube-in-der-neuen-mensa-soll-im-april-2025-wieder-oeffnen-A2H4DGGLOBEATE2CIV7PU2Z6AQ.html)

taff (ProSieben): Deutschland - Deine Straßen (Teil 2): Der historische Studentenclub in Dresden. 16.01.2024 / 17.01.2024

[culTUre-Magazin: „Kino im Kasten“ ist beste alternative Spielstätte. 18.10.2023](https://www.sendcockpit.com/appl/ce/software/code/ext/_ns.php?&uid=551266cea5b108cdf65619dab6a41a05)

[SZ: Nach fast 60 Jahren: Dresdens größter Studentenklub "Club Mensa" ist pleite. 01.09.2023](https://www.saechsische.de/sachsen/gastronomie-sachsen/dresdens-groesster-studentenclub-ist-pleite-5902710.html)

[Campusrauschen: Clubtest: Club 11, 16.06.2023](https://campusrauschen.de/2023/06/16/clubtest-club-11/)

[SZ: Dresdner Bärenzwinger: Striptease auf den Tischen. 10.06.2023](https://www.saechsische.de/dresden/dresden-damals-barenzwinger-studentenklub-ddr-5869948.html)

[Campusrauschen: Clubtest: Gutzkowclub, 10.10.2022](https://campusrauschen.de/2022/10/10/clubtest-gutzkowclub/)

[CAZ: Die Dresdner Studententage sind zurück. 03.05.2022](https://web.caz-lesen.de/die-dresdner-studententage-sind-zurueck/)

[TU Dresden entdecken: Arbeiten, wo Studis feiern – Sitzgelegenheit zu Studentischen Clubs in Dresden. 15.09.2021. (YouTube, 6:00 min)](https://www.youtube.com/watch?v=JulmNHCl5TQ)

[Universitätsjournal: Helfer zu sein war Herzensache, Studentenklubs und Dixieland-Festival, eine Nachbetrachtung, 13.07.2021](https://tu-dresden.de/tu-dresden/newsportal/universitaetsjournal/artikel-uj/helfer-zu-sein-warherzenssache)

[CAZ: Wir haben (trotzdem) Spaß! Dresdner Studententage bis 26. Juni 2021, 22.06.2021](https://web.caz-lesen.de/wir-haben-trotzdem-spass-dresdner-studententage-bis-26-juni-2021/)

[Universitätsjournal: „Der kulturelle Verlust lässt sich nicht in Zahlen ausdrücken“. 08.09.2020.](https://tu-dresden.de/tu-dresden/newsportal/universitaetsjournal/artikel-uj/der-kulturelle-verlust-laesst-sich-nicht-in-zahlen-ausdruecken)

[Spiegel-Ei: Studentenclubs in Zeiten der Krise. 05.07.2020](https://web.caz-lesen.de/files/pdf-archiv/SPIEGEL-EI_2020-3.pdf#page=9)

[SZ: Cotta-Club lädt in neues Domizil ein. 02.02.2020](https://www.saechsische.de/cotta-club-laedt-in-neues-domizil-5167767.html)

Hannelore Stephan: Die Studentenclubs – vom Sektionsclub zum eingetragenen Verein, 2019, In: Rückblick. Einblick. Ausblick : Festschrift zum 100-jährigen Jubiläum des Studentenwerks Dresden

[SZ: Einstige Filmkulisse steht fast leer. 16.04.2019](https://www.saechsische.de/einstige-filmkulisse-steht-fast-leer-5060405.html)

[SZ: Studentenklub sucht neue Bleibe. 14.01.2019](https://www.saechsische.de/studentenklub-sucht-neue-bleibe-5022734.html)

[SZ: Neue Pläne für Tellkamps Filmkulisse. 08.12.2018](https://www.saechsische.de/plus/neue-plaene-fuer-tellkamps-filmkulisse-5008991.html)

[SZ: Die „Öko“ wird verkauft. 06.07.2018](https://www.saechsische.de/die-oeko-wird-verkauft-3970697.html)

[Wochenkurier: Studenten bangen um ihr Wohnheim. 02.07.2018](https://www.wochenkurier.info/sachsen/dresden/artikel/studenten-bangen-um-ihr-wohnheim-52766/)

[DNN: Partys und Kultur – Am Dienstag starten die Dresdner Studententage. 28.05.2018](http://www.dnn.de/Dresden/Lokales/Partys-und-Kultur-Am-Dienstag-starten-die-Dresdner-Studententage)

[SZ: Festung wird FDJ-Studentenclub. 09.06.2018](https://www.saechsische.de/festung-wird-fdj-studentenclub-3952111.html)

[DNN: 50 Jahre Studentenclub Bärenzwinger. 08.06.2018](http://www.dnn.de/Nachrichten/Kultur/Regional/50-Jahre-Studentenclub-Baerenzwinger)

[Campusrauschen: Viel mehr als eine Nachtwanderung. 21.05.2018](https://campusrauschen.de/2018/05/21/viel-mehr-als-eine-nachtwanderung/)

[SZ: Görlitzer Studentenclub hat Ärger mit Besuchern. 23.02.2018](http://www.sz-online.de/sachsen/goerlitzer-studentenclub-hat-aerger-mit-besuchern-3885002.html)

[CAZ: Engagiert: „Studentenclubs sind freie Entfaltung pur“. 30.10.2017](http://caz-lesen.de/campus-news/engagiert-studentenclubs-sind-freie-entfaltung-pur.html)

[CAZ: Sackhüpfen: Trainingsbeginn für Weihnachtsmänner. 17.09.2017](http://caz-lesen.de/campus-news/sackhuepfen-trainingsbeginn-fuer-weihnachtsmaenner.html)

[Tag24: Wofür stehen hier so viele junge Menschen an? 15.05.2017](https://www.tag24.de/nachrichten/schlange-dresden-innenstadt-junge-menschen-nachtwanderung-uni-253952)

[Universitätsjournal: Von Studenten für Studenten. 04.10.2016](https://tu-dresden.de/tu-dresden/newsportal/ressourcen/dateien/universitaetsjournal/uj_pdfs/uj_2016/UJ15-16.pdf?lang=de#page=10)

[CAZ: Freibier auf Stempelkarte. 26.09.2016](http://caz-lesen.de/files/archiv/pdf/caz_ausgabe211_2016-09-26.pdf#page=18)

[Urbanite: Wir können Kultur - 25. Dresdner Studententage. Mai 2016](https://issuu.com/urbanite/docs/urbanite_dresden_mai_2016/26)

[DNN: Kultur am Campus: 25. Studententage in Dresden. 23.05.2016](http://www.dnn.de/Dresden/Boulevard/Kultur-am-Campus-25.-Studententage-in-Dresden)

[CAZ: Endlich Feier-Abend im Studentenclub. 28.09.2015](http://caz-lesen.de/files/archiv/pdf/caz_ausgabe202_2015-09-28.pdf#page=16)

[Campusradio: Interview - 50 Jahre Dresdner Studentenclubs. 27.11.2014](http://campusradiodresden.de/2014/12/03/50-jahre-dresdner-studentenclubs/)

[CAZ: Von Feiermarathon bis Fluthilfe. 27.10.2014](http://www.caz-lesen.de/files/archiv/pdf/caz_ausgabe188_2014-10-27.pdf#page=6)

[CAZ: Es darf erstmal gefeiert werden. 29.09.2014](http://caz-lesen.de/files/archiv/pdf/caz_ausgabe186_2014-09-29.pdf#page=5)

[Universitätsjournal: Von Studenten für Studenten. 06.05.2014](https://tu-dresden.de/tu-dresden/newsportal/ressourcen/dateien/universitaetsjournal/uj_pdfs/uj_2014/UJ08-14.pdf#page=12)

[DNN: Dresden ist Deutschlands Hochburg der Studentenclubs. 30.03.2014](http://www.dnn-online.de/dresden/web/dresden-nachrichten/detail/-/specific/Dresden-ist-Deutschlands-Hochburg-der-Studentenclubs-3299647678)
