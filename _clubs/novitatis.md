---
    name: Studentenclub Novitatis e.V.
    address: Fritz-Löffler-Straße 12c, 01069 Dresden
    city: Dresden
    web: http://novitatis.de
    logo_light: assets/logos/novitatis/light.svg
    logo_dark: assets/logos/novitatis/dark.svg
    lat: 51.0371949
    lon: 13.7313460
    status: member
---
