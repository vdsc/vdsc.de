---
    name: DeZi-Bar e.V.
    address: Schliebenstraße 27, 02763 Zittau
    city: Zittau
    web: http://www.dezi-bar.de
    lat: 50.8911992
    lon: 14.8031304
    status: associate
---
