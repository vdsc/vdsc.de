---
    alias: Objektiv e.V.
    name: Kino im Kasten
    address: August-Bebel-Straße 20, 01219 Dresden
    city: Dresden
    web: https://www.kino-im-kasten.de
    logo_light: assets/logos/kino-im-kasten/light.png
    logo_dark: assets/logos/kino-im-kasten/dark.png
    lat: 51.0302059
    lon: 13.7539671
    status: member
---
