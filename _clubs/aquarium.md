---
    name: Club Aquarium e.V.
    address: St. Petersburger Straße 21, 01069 Dresden
    city: Dresden
    web: https://club-aquarium.de
    logo_light: assets/logos/aquarium/light.svg
    logo_dark: assets/logos/aquarium/dark.svg
    lat: 51.0448464
    lon: 13.7401122
    status: member
---
