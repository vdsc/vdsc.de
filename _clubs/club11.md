---
    name: Club 11 e.V.
    address: Hochschulstraße 48, 01069 Dresden
    city: Dresden
    web: http://www.clubelf.de
    logo_light: assets/logos/club11/light.png
    logo_dark: assets/logos/club11/dark.png
    lat: 51.0319016
    lon: 13.7307345
    status: member
---
