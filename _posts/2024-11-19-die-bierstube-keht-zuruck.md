---
layout: post
title:  Die Bierstube kehrt zurück!
date:   2024-11-19 17:00
author: burts
---

Knapp 10 Jahre nach der Schließung der Neuen Mensa und damit auch der Bierstube soll es im nächsten Jahr an der Bergstraße wieder einen studentischen Kulturbetrieb geben.
Das [Studentenwerk Dresden][1], der [StuRa][2] und der [VDSC][3] haben dazu eine Absichtserklärung beschlossen. Der Barbetrieb der Bierstube soll dabei durch das StuWe erfolgen. Für den Kulturbetrieb in der Bierstube sowie den Räumen der Mensa wird ein neuer studentischer Kulturverein gegründet, welcher aus den Erlösen des Bierstubenbetriebs mitfinanziert wird.

[1]: https://www.studentenwerk-dresden.de/kultur/
[2]: https://www.stura.tu-dresden.de/
[3]: https://vdsc.de/verein.html
