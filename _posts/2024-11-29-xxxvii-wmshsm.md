---
layout: post
title:  XXXVII. Weihnachstmannsackhüpfstaffelmarathon
date:   2024-11-29 18:00
author: burts
---

Am 30. November findet im, am und um den [Club Bärenzwinger][1] wieder der Weihnachstmannsackhüpfstaffelmarathon statt. Mitglieder von Studentenclubs aus ganz Deutschland treffen sich, um vorweihnachtlich kostümiert die 10×100 Meter lange Strecke von der Brühlschen Terrasse bis zum Terrassenufer sackhüpfend zu absolvieren und davor, währenddessen und danach zu feiern.

[1]: https://baerenzwinger.de/