---
layout: post
title:  "Feste Feiern im November"
date:   2014-11-01 12:00
author: Marc
---

Endlich ist er da, der November: einen ganzen Monat [Feste Feiern][13] lautet die Devise, denn die Dresdner Studentenclubs wollen ihr 50-jähriges Jubiläum mit euch zusammen ordentlich die Korken knallen lassen.

[13]: /50#festefeiern
