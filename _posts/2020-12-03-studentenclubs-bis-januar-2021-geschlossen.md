---
layout: post
title:  "Studentenclubs bis Januar 2021 geschlossen"
date:   2020-12-03 17:30
author: Burts
---

Der aktuelle Teil-Lockdown wird, wie gestern bekanntgegeben, bis mindestens 10. Januar 2021 verlängert. Die bereits seit Ende Oktober 2020 andauernde erneute Schließung der Dresdner Studentenclubs wird dann fast drei Monate umfassen und trifft die Clubs ähnlich hart wie bereits der erste Lockdown im Frühjahr. Wir bedanken uns beim [Dresdner Studentenwerk][1] für die Unterstützung und hoffen das alle Clubs diese Zeit gut überstehen.
Leider muss in diesem Jahr auch der [Weihnachtsmannsackhüpfstaffelmarathon][2] ausfallen, bei dem sich sonst Studentenclubs aus ganz Deutschland in Dresden treffen.
Wir wünschen dennoch eine schöne Adventszeit und hoffen im nächsten Jahr wieder für die Dresdner Studentenschaft öffnen zu können.

[1]: https://www.studentenwerk-dresden.de/kultur/
[2]: https://www.baerenzwinger.de/geschichte/