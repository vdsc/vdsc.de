---
layout: post
title:  "Dresdner Studententage 2018"
date:   2018-05-29 12:00
author: Burts
---

Heute beginnen die [Dresdner Studententage 2018][1] mit dem Kulturmarkt an der Alten Mensa Mommsenstraße. Highlights sind unter anderem der UniAir-Bandcontest, die [Nachtwanderung][2], das MittelAlterFest, der UniSlam und TUinSzene. Aber auch die vielen kleinen Veranstaltungen bieten ein abwechslungsreiches Programm vom 29. Mai bis 15. Juni.

[1]: https://www.dresdner-studententage.de/ 
[2]: https://www.dresdner-nachtwanderung.de/