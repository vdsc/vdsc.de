---
layout: post
title:  "Vorerst noch kein Clubbetrieb"
date:   2020-05-20 19:00
author: Burts
---

Inzwischen sind die Corona-Einschränkungen wieder etwas gelockert worden. Für die Studentenclubs ist ein Clubbetrieb aktuell jedoch noch nicht wieder möglich. An Alternativen wird bereits gearbeitet.
So wird zum Feiertag der [Bärenzwinger][1] zum Beispiel mit einem Biergarten und der [Club 11][2] mit einem Straßenverkauf öffnen!

[1]: https://www.baerenzwinger.de/
[2]: https://www.clubelf.de/