---
layout: post
title:  "ESE-Clubtour 2021"
date:   2021-10-05 00:00
author: Burts
---
Zum Start des Wintersemesters laden die [Studentenclubs][1] zusammen mit den [Fachschaftsräten][2] heute wieder die neuen Studenten zur Erstsemester-Tour durch die Studentenclubs. Die Clubs haben, unter 3G-Corona-Auflagen, wieder halbwegs regulär geöffnet.

[1]: https://vdsc.de/clubs
[2]: https://www.stura.tu-dresden.de/fachschaften
