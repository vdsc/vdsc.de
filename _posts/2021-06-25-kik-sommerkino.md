---
layout: post
title:  "KiK-Sommerkino"
date:   2021-06-25 18:00
author: Burts
---
Das [Kino im Kasten][1] läd ab heute bis zum 23. Juli immer Freitags zum [Sommerkino][2] auf die Wiese hinter dem Hörsaalzentrum. Die Veranstaltung wird unterstützt durch den StuRa sowie gastronomisch durch den VDSC.

[1]: https://www.kino-im-kasten.de/
[2]: https://tu-dresden.de/tu-dresden/universitaetskultur/campusleben/sommerkino-2021
