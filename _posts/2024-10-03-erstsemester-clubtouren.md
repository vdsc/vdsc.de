---
layout: post
title:  Erstsemeter-Clubtouren zum Semesterstart
date:   2024-10-03 00:00
author: burts
---

Zum Semesterstart an der HTW Dresden fand am 25. September die ESE-Clubtour in der Erstsemester-Einführungswoche des [HTW-Stura][1] statt. In der nächsten Woche findet am 8. Oktober die ESE-Clubtour für die neuen Studierenden der TU Dresden in Kooperation mit den [TU-Fachschaftsräten][2] statt.

[1]: https://www.stura.htw-dresden.de/
[2]: https://www.stura.tu-dresden.de/fachschaften
