---
layout: post
title:  "Heinrich-Cotta-Club eröffnet neues Clubhaus in Tharandt"
date:   2019-12-03 11:15
author: Burts
---

Nun ist es endlich soweit! Nach langem Suchen, Bangen, Bauen und Räumen öffnet der [Heinrich-Cotta-Club][1] in Tharandt [heute abend][2] erstmals sein neues Clubhaus auf der Wilsdruffer Straße 20.
Die Dresdner Studentenclubs freuen sich, dass es nun doch endlich alles so gut geklappt, und freuen sich auf viele schöne Abende im neuen Häuschen…

[1]: http://www.heinrich-cotta-club.de/
[2]: http://www.heinrich-cotta-club.de/veranstaltungen.html
