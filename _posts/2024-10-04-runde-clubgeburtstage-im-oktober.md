---
layout: post
title:  Runde Clubgeburtstage im Oktober
date:   2024-10-04 00:00
author: burts
---

Im Monat Oktober stehen in diesem Jahr mehrere große Clubgeburtstage an. Den
Auftakt macht der Görlitzer [Studierendenclub Maus][1], der am 2. Oktober stolz
sein 50-jähriges Bestehen feiert. Am 4. Oktober feiert der [Kellerklub GAG18][2]
ebenfalls seinen 50. Geburtstag. Und das ist noch nicht alles: Im [Gutzkowclub][3]
findet vom 14. bis 19. Oktober eine ganze Festwoche zum 60. Geburtstag statt!
Zum Abschluss steht am 25. Oktober schließlich der immerhin 41. Geburtstag des
[Club Aquarium][4] ins Haus.

Wir wünschen allen Clubs auch weiterhin engagierte Mitglieder und interessierte
Gäste und viele weitere aktive Jahre.

[1]: https://www.maus-goerlitz.de/
[2]: https://www.gag-18.com/
[3]: http://gutzkowclub.de/
[4]: https://club-aquarium.de/
