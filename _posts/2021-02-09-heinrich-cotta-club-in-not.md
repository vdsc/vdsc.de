---
layout: post
title:  "Heinrich-Cotta-Club in Not"
date:   2021-02-09 17:50
author: Burts
---

Fast ein Jahr Corona geht auch an den Studentenclubs nicht spurlos vorbei. Besonders kritisch sieht es derzeit beim [Heinrich-Cotta-Club e.&nbsp;V.][1] in Tharandt aus – zumal der Club erst Ende 2019 sein neues Domizil bezogen hatte. Daher [ruft der Verein nun zu Spenden auf][2], um die laufenden Kosten zu decken, und sein wirtschaftliches Überleben zu sichern.

[1]: http://heinrich-cotta-club.de/
[2]: https://www.betterplace.org/de/projects/90342-rettung-tharandt-s-letzter-studentenkneipe
