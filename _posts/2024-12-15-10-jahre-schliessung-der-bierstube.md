---
layout: post
title:  10 Jahre Schließung der Bierstube
date:   2024-12-15 14:00
author: burts
---

Am 14. Dezember 2014 hatte die Bierstube in der Neuen Mensa zum letzten Mal geöffnet. Den Veranstaltungsbetrieb in den Mensa-Räumen hatte der Klub Neue Mensa da schon seit Monaten einstellen müssen. Seitdem fehlt etwas im Herzen des Campus der TU Dresden.

Das war vor 10 Jahren. Letztlich fiel die Mensa erstmal in einen Dornröschenschlaf und es dauerte noch eine lange Zeit bis tatsächlich Bautätigkeiten begonnen haben. Nun geht die Sanierung der Mensa endlich dem Ende entgegen. Wir freuen uns, dass es im nächsten Jahr wieder die Möglichkeit geben soll, Veranstaltungen in der Mensa durchzuführen. Auch die Bierstube soll wieder öffnen. Den Klub Neue Mensa e.V. gibt es leider nicht mehr. Stattdessen soll der Betrieb der Bierstube nun in Kooperation zwischen dem  [Studentenwerk][1] und einen neuen studentischen Kulturverein stattfinden, welcher auch Veranstaltungen in den Sälen und dem Foyer durchführen wird. Details dazu planen derzeit die [Vereinigung Dresdner Studentenclubs][2] und der [StuRa der TU Dresden][3].

Für alle Interessierten soll es dazu morgen (Mo, 16.12.2024) ab 16:40 Uhr in der Alten Mensa (Westsaal) eine [Informationsveranstaltung][4] geben.

[1]: https://www.studentenwerk-dresden.de/kultur/
[2]: https://vdsc.de/verein.html
[3]: https://www.stura.tu-dresden.de/
[4]: https://www.stura.tu-dresden.de/kalender/241202_bier_und_musik_zur%C3%BCck_die_neue_mensa