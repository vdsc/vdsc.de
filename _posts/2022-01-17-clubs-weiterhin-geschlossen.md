---
layout: post
title:  "Clubs weiterhin geschlossen"
date:   2022-01-17 13:00
author: Burts
---
Trotz gesunkener Infektionszahlen bleibt die Lage angespannt, und der aktuelle Shutdown dauert an. Mit dem neuen 2Gplus-Konzept können einige gastronomische Einrichtungen zwar öffnen – die Studentenclubs bleiben aber weiterhin geschlossen. Wir hoffen, dass sich die Lage mittelfristig entspannt, damit wir bald wieder für Euch öffnen können.
