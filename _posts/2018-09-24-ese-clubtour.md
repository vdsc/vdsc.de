---
layout: post
title:  "ESE-Clubtour"
date:   2018-09-24 12:00
author: Burts
---

Zum Beginn des Wintersemesters gibt es für alle Erstsemester am 02.10.2018 wieder die ESE-Clubtour als Teil der Erstsemester-Einführung. Gemeinsam mit den Fachschaften von TU und HTW laden die Dresdner Studentenclubs wieder alle Erstis zur Schuppertour durch die Clubs. Im vierten Club wartet sogar ein Freigetränk… Alles weitere dazu steht bei [eXma][7].


[7]: /eseclubtour
