---
layout: post
title:  "ESE-Clubtour"
date:   2015-10-05 12:00
author: Marc
---

Zum Beginn des Wintersemesters möchten euch die Dresdner Studentenclubs ganz herzlich zur [ESE-Clubtour][7] einladen! An einem Abend habt ihr die Chance, möglichst viele unserer Clubs kennenzulernen. Kommt vorbei, wir freuen uns auf euch!

[7]: /eseclubtour
