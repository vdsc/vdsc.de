---
layout: post
title:  "Heute ist Nachtwanderung"
date:   2015-05-05 12:00
author: Marc
---


Schnürt die Wanderstiefel und kommt heute abend zur [16. Dresdner Nachtwanderung][8]! Konzerte und Partys in 14 Clubs, Bustransfer, Freigetränke und viele ehrenamtliche Mitglieder der Studentenclubs warten auf euch!

[8]: https://www.dresdner-nachtwanderung.de/2015/
