---
layout: post
title:  "Corona-Virus: Studentenclubs bleiben vorerst geschlossen"
date:   2020-03-17 14:30
author: Burts
---

Entsprechend der beschlossenen Maßnahmen zur Eindämmung der COVID-19-Pandemie sind die Dresdner Studentenclubs bis auf Weiteres geschlossen. Der Semesterbeginn der Dresdner Hochschulen ist vorerst auf Mai verschoben.

Es muss jedoch davon ausgegangen werden, dass die für Mai geplanten Dresdner Studententage nicht stattfinden werden. Ob diese auf einen späteren Zeitpunkt im Jahr 2020 verschoben werden können, wird in den nächsten Wochen entsprechend der aktuellen Lage geprüft.
