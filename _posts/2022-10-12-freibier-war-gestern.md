---
layout: post
title:  "Freibier war gestern"
date:   2022-10-12 9:30
author: Burts
---

Am Dienstag den 11.10. haben die Dresdner Studentenclubs zum Semesterstart an alle Studierenden vor dem Hörsaalzentrum der TU Dresden Freibier verschenkt und Infomaterial verteilt. Natürlich nur verschlossene Flaschen zu Verzehr nach Vorlesungsende! Wir bedanken uns bei Freiberger und Splendid Drinks für die Unterstützung.
