---
layout: post
title:  "Weitere Clubs beigetreten"
date:   2018-12-31 12:00
author: Burts
---

Nachdem auch in den Studentenclubs eine kurze weihnachtliche Ruhepause eingekehrt ist, ist Zeit zurückzublicken. Nachdem im vergangenen Jahr noch der Gutzkowclub, der Bärenzwinger und das Kino im Kasten dem Verein beigetreten sind, haben wir nunmehr 13 Mitglieder. Damit sind fast alle Dresdner Studentenclubs im VDSC organisiert. Wir freuen uns auf ein aktives Jahr 2019!

