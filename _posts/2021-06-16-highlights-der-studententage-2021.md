---
layout: post
title:  "Highlights der Studententage 2021"
date:   2021-06-16 22:30
author: Burts
---
Aufgrund der inzwischen stark gesunkenen Inzidenz ist nun zu den [Dresdner Studententagen][1] doch mehr möglich als gedacht! In dieser Woche warten als Highlights am Freitag ein Konzert im Innenhof des [Club Bärenzwinger][2] und am Sonnabend ein [Picknickkonzert][3] hinter dem [Club Novitatis][4]. In der nächsten Woche gibt es unter anderem ein Kneipenquiz im [Club Aquarium][5] und eine kleine Ersatzveranstaltung für das [MittelAlterFest][6].

[1]: https://dresdner-studententage.de/
[2]: https://www.baerenzwinger.de/
[3]: https://wu5.de/node/290
[4]: http://novitatis.de/
[5]: https://club-aquarium.de/
[6]: https://www.mittelalterfeste.eu/
