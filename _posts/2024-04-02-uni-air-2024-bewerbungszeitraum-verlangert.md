---
layout: post
title:  "UNI AIR 2024 – Bewerbungszeitraum bis 14. April verlängert"
date:   2024-04-02
author: Burts
---
Studentische Bands und DJs können noch bis Mitte April die Chance zur Bewerbung nutzen, um beim UNI AIR 2024 dabei zu sein!

Im Rahmen der [Dresdner Studententage 2024][1] veranstalten das [Studentenwerk Dresden][2] und die [Dresdner Studentenclubs][3] den studentischen Musikwettbewerb [UNI AIR][4] für SolistInnen/Bands und DJs. Sechs SolistInnen/Bands und sechs DJs erhalten am 12. Juni die Chance, sich einem großen Publikum auf der Wiese hinter dem Hörsaalzentrum der TU Dresden zu präsentieren.

Eine große Bühne, professionelle Technik und ein fantastisches Publikum warten auf euch. Dazu haben die Teilnehmer die Chance auf einen der attraktiven Förderpreise des Studentenwerks Dresden von bis zu 1.300 Euro. Der Bewerbungszeitraum auf einen Startplatz beim diesjährigen [UNI AIR][4] wurde [verlängert][5] – diese ist noch bis zum 14. April möglich.

[1]: https://dresdner-studententage.de/
[2]: https://studentenwerk-dresden.de/kultur
[3]: https://vdsc.de/clubs
[4]: https://uni-air.de
[5]: https://www.studentenwerk-dresden.de/wirueberuns/newsartikel-5993.html
