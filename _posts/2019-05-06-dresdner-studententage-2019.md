---
layout: post
title:  "Dresdner Studententage 2019"
date:   2019-05-06 15:00
author: Burts
---

Am Dienstag beginnen die [Dresdner Studententage 2019][1]. Zum Start gibt es über die Mittagszeit den Kulturmarkt an der Mensa Zeltschlösschen. Vom 7. bis 28. Mai erwartet euch dann ein abwechslungsreiches Programm mit Höhenpunkten wie dem Uni-Air-Bandcontest, der [20. Dresdner Nachtwanderung][2] sowie dem [Mittelalterfest][3]. Aber auch die vielen kleineren Veranstaltungen der Studentenclubs sind mehr als einen Blick in das Programm wert.

[1]: https://www.dresdner-studententage.de/
[2]: https://www.dresdner-nachtwanderung.de/
[3]: https://www.mittelalterfeste.eu/