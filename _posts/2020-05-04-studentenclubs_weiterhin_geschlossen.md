---
layout: post
title:  "Studentenclubs weiterhin geschlossen"
date:   2020-05-04 17:50
author: Burts
---

Aufgrund der Verlängerung der Maßnahmen zur Eindämmung der COVID-19-Pandemie bleiben die Dresdner Studentenclubs vorerst auch weiterhin geschlossen! Wir hoffen, dass sich die Lage dennoch bald entspannt. Bis dahin werden die Mitglieder der Dresdner Studentenclubs alles daran setzen, das wirtschaftliche Überleben der Clubs zu sichern, um euch auch nach der Corona-Schließung wieder ein breites studentisches Kulturprogramm bieten zu können.