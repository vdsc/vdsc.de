---
layout: post
title:  Bier und Musik - Zurück in die Neue Mensa!
date:   2024-12-04 16:00
author: burts
---

Mit der Wiedereröffung der Neuen Mensa an der Bergstraße soll auch wieder ein Kulturbetrieb in die Bierstube, das Foyer und die Mensasäle einziehen. Für alle, die sich einbringen möchten oder die Bierstube schon lange vermissen, gibt es gute Nachrichten: Am [Montag, den 16. Dezember 2024, ab 16:40 Uhr][1] lädt die Projektgruppe Bierstube des StuRa der TU Dresden und des  VDSC e.V. zu einer Infoveranstaltung in der Alten Mensa (Westsaal, Zugang Helmholtzstraße) ein. Im Fokus steht die Frage, wie Bier und Kultur ihren Platz in der Bierstube und der Neuen Mensa finden können. Zudem bietet die Veranstaltung Raum für Ideen, Wünsche und Visionen für die neue Bierstube.

[1]: https://www.stura.tu-dresden.de/kalender/241202_bier_und_musik_zur%C3%BCck_die_neue_mensa