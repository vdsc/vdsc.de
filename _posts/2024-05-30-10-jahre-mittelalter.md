---
layout: post
title:  "Das MittelAlterFest feiert 10-jähriges Jubiläum!"
date:   2024-05-30 11:00
author: Lukas
---

Dieses Jahr feiert das MittelAlterFest, organisiert von den Dresdner Studentenclubs [Traumtänzer](https://club-traumtaenzer.de/) und [Kellerklub GAG 18](https://www.gag-18.com/), sein 10-jähriges Jubiläum. Anlässlich dieses runden Geburtstags wird das Fest um einen vierten Tag erweitert und startet am 30. Mai mit dem Motto „Mehr Punk als Mittelalter“. Vier Bands aus der Dresdner Studentenclubszene sorgen an diesem Tag für Stimmung. Das Markttreiben bleibt dabei bestehen, da fast alle Händler auch schon am Donnerstag ihre Stände öffnen.

Das MittelAlterFest findet vom 30. Mai bis 2. Juni 2024 auf der [Wiese hinter dem Kellerklub GAG 18/Studentenwerk](http://www.openstreetmap.org/?mlat=51.0358849&mlon=13.7306448&zoom=16) statt. Die ehrenamtlichen Mitglieder setzen sich dafür ein, ein vielfältiges Programm zu günstigen Preisen anzubieten, und arbeiten dabei eng mit anderen Vereinen und Helfern zusammen.

Weitere Informationen findet ihr unter [www.mittelalterfeste.eu](https://www.mittelalterfeste.eu/).
