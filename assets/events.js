"use strict";
async function create_event_table(caption, days) {
    async function fetch_events() {
        const resp = await fetch("https://events.vdsc.de/calendar.json");
        try {
            if(!resp.ok) {
                throw new Error(`${resp.status} ${resp.statusText}`);
            }
            return [await resp.json(), resp.headers.get("Last-Modified")];
        } catch(e) {
            e.message = `Veranstaltungen konnten nicht geladen werden: ${e.message}`;
            throw e;
        }
    }

    function inc_day(d) {
        let new_d = d;
        do {
            // let's hope no-one changes their DST by more than 1 hours
            new_d = new Date(new_d.getTime() + 23 * 3600);
        } while(new_d.getDate() == d.getDate());
        new_d.setHours(0);
        new_d.setMinutes(0);
        new_d.setSeconds(0);
        new_d.setMilliseconds(0);
        return new_d;
    }

    function group_events(events, cut_off) {
        const by_year = new Map();
        for(const ev of events) {
            const dtstart = new Date(ev["dtstart"]);
            if(cut_off !== undefined && dtstart >= cut_off) {
                continue;
            }

            const year = dtstart.getFullYear();
            if(!by_year.has(year)) {
                by_year.set(year, new Map());
            }
            const by_month = by_year.get(year);

            const month = dtstart.getMonth() + 1;
            if(!by_month.has(month)) {
                by_month.set(month, new Map());
            }
            const by_day = by_month.get(month);

            const day = dtstart.getDate();
            if(!by_day.has(day)) {
                by_day.set(day, []);
            }
            const day_events = by_day.get(day);
            day_events.push(ev);
        }
        return by_year;
    }

    function compare_numbers(a, b) {
        return (a > b) - (a < b);
    }

    function iter_keys(m) {
        return Array.from(m.keys()).sort(compare_numbers);
    }

    function compare_events(a, b) {
        const at = Date.parse(a["dtstart"]);
        const bt = Date.parse(b["dtstart"]);
        if(at < bt) {
            return -1;
        } else if(at > bt) {
            return 1;
        }
        for(const key of ["location", "summary"]) {
            const av = a[key];
            const bv = b[key];
            if(av && bv) {
                if(av < bv) {
                    return -1;
                } else if(av > bv) {
                    return 1;
                }
            }
        }
        return 0;
    }

    function render_event(table, ev, dtstart) {
        const time_formatter = new Intl.DateTimeFormat("de-DE", {
            hour: "2-digit",
            minute: "2-digit",
        });

        if(ev["dtstart"].indexOf("T") >= 0) {
            // if DTSTART did not include a time, it's an all-day event
            const td_dt = table.appendChild(document.createElement("div"));
            td_dt.classList.add("time");
            const dt = td_dt.appendChild(document.createElement("time"));
            dt.setAttribute("datetime", ev["dtstart"]);
            dt.appendChild(
                document.createTextNode(
                    time_formatter.format(dtstart)
                )
            );
        }

        const td_summary = table.appendChild(document.createElement("div"));
        td_summary.classList.add("summary");
        const a = td_summary.appendChild(document.createElement("a"));
        a.href = ev["url"];
        a.appendChild(document.createTextNode(ev["summary"]));

        if(ev["location"]) {
            const td_location = table.appendChild(document.createElement("div"));
            td_location.classList.add("location");
            const addr = td_location.appendChild(document.createElement("addr"));
            addr.title = ev["location"];
            addr.appendChild(
                document.createTextNode(
                    ev["location"].match(/^[^\n,]*/)?.[0] || ev["location"]
                )
            );
        }
    }

    function populate_table(table, events, last_modified, days) {
        const short_date_formatter = new Intl.DateTimeFormat("de-DE", {
            weekday: "short",
            day: "numeric",
            month: "short",
        });
        const long_date_formatter = new Intl.DateTimeFormat("de-DE", {
            weekday: "short",
            day: "numeric",
            month: "short",
            year: "numeric",
        });

        days = days || 14;
        let last_day = new Date();
        while(days-- > 1) {
            last_day = inc_day(last_day);
        }
        const cut_off = inc_day(last_day);

        const by_year = group_events(events, cut_off);
        let num_events = 0;
        for(const year of iter_keys(by_year)) {
            const by_month = by_year.get(year);
            for(const month of iter_keys(by_month)) {
                const by_day = by_month.get(month);
                for(const day of iter_keys(by_day)) {
                    const day_evs = by_day.get(day);

                    let rowspan = day_evs.length;
                    for(const ev of day_evs.sort(compare_events)) {
                        const dtstart = Date.parse(ev["dtstart"]);

                        const reset = table.appendChild(document.createElement("div"));
                        if(rowspan > 0) {
                            reset.classList.add("new-day");
                            for(const [cls, fmt] of [
                                ["long", long_date_formatter],
                                ["short", short_date_formatter],
                            ]) {
                                const td = table.appendChild(document.createElement("div"));
                                td.classList.add("date");
                                td.classList.add(cls);
                                td.appendChild(
                                    document.createTextNode(
                                        fmt.format(dtstart)
                                    )
                                );
                            }
                            rowspan = 0;
                        } else {
                            reset.classList.add("new-event");
                        }

                        render_event(table, ev, dtstart);
                        ++num_events;
                    }
                }
            }
        }

        if(last_modified) {
            last_modified = new Date(last_modified);
            const div = table.appendChild(document.createElement("div"));
            div.classList.add("caption");
            div.appendChild(
                document.createTextNode(`Stand: ${new Intl.DateTimeFormat("de-DE", {dateStyle: "long", timeStyle: "long"}).format(last_modified)}`)
            );
        }

        if(num_events == 0) {
            return `keine Veranstaltungen bis ${new Intl.DateTimeFormat("de-DE", {dateStyle: "long"}).format(last_day)}`;
        }
    }

    const error = caption;
    const table = error.parentNode;
    try {
        const [events, last_modified] = await fetch_events();
        try {
            const message = populate_table(table, events, last_modified, days);
            if(message !== undefined) {
                error.textContent = message;
            } else {
                table.removeChild(error);
            }
        } catch(e) {
            e.message = `Veranstaltungen konnten nicht dargestellt werden: ${e.message}`;
            throw e;
        }
    } catch(e) {
        error.textContent = String(e.message);
        throw e;
    }
}
